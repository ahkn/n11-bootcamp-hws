## Output

```agsl
Total price of apartments: 3238257.53
Total price of villas: 3121184.58
Total price of resorts: 3500665.16
Total price of all housing: 9860107.27

Average area of apartments: 107.98322128227153
Average area of villas: 144.5714195126074
Average area of resorts: 143.06775785661955
Average area of all housing: 131.87413288383286

Grouped by bedrooms and living rooms:
Bedrooms: 1, Living Rooms: 1, Count: 1
Bedrooms: 2, Living Rooms: 1, Count: 4
Bedrooms: 3, Living Rooms: 1, Count: 2
Bedrooms: 3, Living Rooms: 2, Count: 1
Bedrooms: 5, Living Rooms: 1, Count: 3
Bedrooms: 5, Living Rooms: 2, Count: 1
Bedrooms: 6, Living Rooms: 1, Count: 1
Bedrooms: 7, Living Rooms: 1, Count: 2
```