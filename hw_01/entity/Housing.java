package entity;

import java.math.BigDecimal;

// Housing is a general interface for all housing types
// All housing types should implement this interface
// The service depends on this interface
public interface Housing {
    void setArea(double area);

    double getArea();

    void setLivingRooms(int livingRooms);

    int getLivingRooms();

    void setBedrooms(int bedrooms);

    int getBedrooms();

    void setPrice(BigDecimal price);

    BigDecimal getPrice();
}
