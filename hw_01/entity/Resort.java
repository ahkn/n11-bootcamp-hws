package entity;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Resort implements Housing {
    double area = 0;
    int livingRooms = 0;
    int bedrooms = 0;
    BigDecimal price = new BigDecimal(0);

    @Override
    public void setArea(double area) {
        if (area <= 0) {
            throw new IllegalArgumentException("Area must be a positive number");
        }

        this.area = area;
    }

    @Override
    public double getArea() {
        return this.area;
    }

    @Override
    public void setLivingRooms(int livingRooms) {
        if (livingRooms < 0) {
            throw new IllegalArgumentException("Living rooms must be a positive number");
        }

        this.livingRooms = livingRooms;
    }

    @Override
    public int getLivingRooms() {
        return this.livingRooms;
    }

    @Override
    public void setBedrooms(int bedrooms) {
        if (bedrooms < 0) {
            throw new IllegalArgumentException("Bedrooms must be a positive number");
        }

        this.bedrooms = bedrooms;
    }

    @Override
    public int getBedrooms() {
        return this.bedrooms;
    }

    @Override
    public void setPrice(BigDecimal price) {
        if (price.compareTo(BigDecimal.ZERO) < 0) {
            throw new IllegalArgumentException("Price must be a positive number");
        }

        this.price = price.setScale(2, RoundingMode.HALF_UP);
    }

    @Override
    public BigDecimal getPrice() {
        return this.price;
    }
}
