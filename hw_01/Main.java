import entity.Housing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        Service service = new Service();

        List<Housing> apartments = service.createListOfApartments(5);
        List<Housing> villas = service.createListOfVillas(5);
        List<Housing> resorts = service.createListOfResorts(5);

        List<List<Housing>> allLists = new ArrayList<>();
        allLists.add(apartments);
        allLists.add(villas);
        allLists.add(resorts);

        System.out.println("Total price of apartments: " + service.getTotalPriceOfApartments(apartments));
        System.out.println("Total price of villas: " + service.getTotalPriceOfVillas(villas));
        System.out.println("Total price of resorts: " + service.getTotalPriceOfResorts(resorts));
        System.out.println("Total price of all housing: " + service.getTotalPriceOfLists(apartments, villas, resorts));

        System.out.println("\nAverage area of apartments: " + service.getAverageAreaOfApartments(apartments));
        System.out.println("Average area of villas: " + service.getAverageAreaOfVillas(villas));
        System.out.println("Average area of resorts: " + service.getAverageAreaOfResorts(resorts));
        System.out.println("Average area of all housing: " + service.getAverageAreaOfLists(apartments, villas, resorts));

        HashMap<Integer, HashMap<Integer, List<Housing>>> grouped = service.groupByBedroomsAndLivingRooms(allLists);

        System.out.println("\nGrouped by bedrooms and living rooms:");
        for (Integer bedrooms : grouped.keySet()) {
            for (Integer livingRooms : grouped.get(bedrooms).keySet()) {
                System.out.println("Bedrooms: " + bedrooms + ", Living Rooms: " + livingRooms + ", Count: " + grouped.get(bedrooms).get(livingRooms).size());
            }
        }
    }
}
