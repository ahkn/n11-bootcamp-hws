import entity.Apartment;
import entity.Housing;
import entity.Resort;
import entity.Villa;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Service {
    public List<Housing> createListOfApartments(int count) {
        List<Housing> apartments = new ArrayList<>();

        for (int i = 0; i < count; i++) {
            Apartment house = new Apartment();
            house.setArea(50 + (Math.random() * 100));
            house.setBedrooms(1 + (int) (Math.random() * 3));
            house.setLivingRooms(1 + (int) (Math.random() * 1));

            house.setPrice(BigDecimal.valueOf(100000 + (Math.random() * 1000000)));
            apartments.add(house);
        }

        return apartments;
    }

    public List<Housing> createListOfVillas(int count) {
        List<Housing> villas = new ArrayList<>();

        for (int i = 0; i < count; i++) {
            Villa house = new Villa();
            house.setArea(75 + (Math.random() * 100));
            house.setBedrooms(2 + (int) (Math.random() * 4));
            house.setLivingRooms(1 + (int) (Math.random() * 2));
            house.setPrice(BigDecimal.valueOf(200000 + (Math.random() * 1000000)));
            villas.add(house);
        }

        return villas;
    }

    public List<Housing> createListOfResorts(int count) {
        List<Housing> resorts = new ArrayList<>();

        for (int i = 0; i < count; i++) {
            Resort house = new Resort();
            house.setArea(100 + (Math.random() * 100));
            house.setBedrooms(3 + (int) (Math.random() * 5));
            house.setLivingRooms(1 + (int) (Math.random() * 1));
            house.setPrice(BigDecimal.valueOf(300000 + (Math.random() * 1000000)));
            resorts.add(house);
        }

        return resorts;
    }

    public BigDecimal getTotalPriceOfApartments(List<Housing> housingList) {
        BigDecimal totalPrice = new BigDecimal(0);

        for (Housing housing : housingList) {
            if (housing instanceof Apartment) {
                totalPrice = totalPrice.add(housing.getPrice());
            }
        }

        return totalPrice;
    }

    public BigDecimal getTotalPriceOfVillas(List<Housing> housingList) {
        BigDecimal totalPrice = new BigDecimal(0);

        for (Housing housing : housingList) {
            if (housing instanceof Villa) {
                totalPrice = totalPrice.add(housing.getPrice());
            }
        }

        return totalPrice;
    }

    public BigDecimal getTotalPriceOfResorts(List<Housing> housingList) {
        BigDecimal totalPrice = new BigDecimal(0);

        for (Housing housing : housingList) {
            if (housing instanceof Resort) {
                totalPrice = totalPrice.add(housing.getPrice());
            }
        }

        return totalPrice;
    }

    @SafeVarargs
    public final BigDecimal getTotalPriceOfLists(List<Housing>... housingLists) {
        BigDecimal totalPrice = new BigDecimal(0);

        for (List<Housing> housingList : housingLists) {
            for (Housing housing : housingList) {
                totalPrice = totalPrice.add(housing.getPrice());
            }
        }

        return totalPrice;
    }

    public double getAverageAreaOfApartments(List<Housing> housingList) {
        double totalArea = 0;
        int count = 0;

        for (Housing housing : housingList) {
            if (housing instanceof Apartment) {
                totalArea += housing.getArea();
                count++;
            }
        }

        return totalArea / count;
    }

    public double getAverageAreaOfVillas(List<Housing> housingList) {
        double totalArea = 0;
        int count = 0;

        for (Housing housing : housingList) {
            if (housing instanceof Villa) {
                totalArea += housing.getArea();
                count++;
            }
        }

        return totalArea / count;
    }

    public double getAverageAreaOfResorts(List<Housing> housingList) {
        double totalArea = 0;
        int count = 0;

        for (Housing housing : housingList) {
            if (housing instanceof Resort) {
                totalArea += housing.getArea();
                count++;
            }
        }

        return totalArea / count;
    }

    @SafeVarargs
    public final double getAverageAreaOfLists(List<Housing>... housingLists) {
        double totalArea = 0;

        for (List<Housing> housingList : housingLists) {
            for (Housing housing : housingList) {
                totalArea += housing.getArea();
            }
        }

        return totalArea / (housingLists.length * housingLists[0].size());
    }

    public HashMap<Integer, HashMap<Integer, List<Housing>>> groupByBedroomsAndLivingRooms(List<List<Housing>> housingList) {
        HashMap<Integer, HashMap<Integer, List<Housing>>> grouped = new HashMap<>();

        for (List<Housing> list : housingList) {
            for (Housing housing : list) {
                int bedrooms = housing.getBedrooms();
                int livingRooms = housing.getLivingRooms();

                if (!grouped.containsKey(bedrooms)) {
                    grouped.put(bedrooms, new HashMap<>());
                }

                if (!grouped.get(bedrooms).containsKey(livingRooms)) {
                    grouped.get(bedrooms).put(livingRooms, new ArrayList<>());
                }

                grouped.get(bedrooms).get(livingRooms).add(housing);
            }
        }

        return grouped;
    }
}
