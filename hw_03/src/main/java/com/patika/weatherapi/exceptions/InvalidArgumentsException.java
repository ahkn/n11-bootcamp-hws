package com.patika.weatherapi.exceptions;

import com.patika.weatherapi.general.BaseErrorMessage;
import com.patika.weatherapi.general.GenericException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)

public class InvalidArgumentsException extends GenericException {
    public InvalidArgumentsException(BaseErrorMessage baseErrorMessage) {
        super(baseErrorMessage);
    }
}
