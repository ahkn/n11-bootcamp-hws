package com.patika.weatherapi.general;

import java.io.Serializable;

public interface BaseErrorMessage extends Serializable {

    String getMessage();
}
