package com.patika.weatherapi.controller;

import com.patika.weatherapi.dto.ForecastResponse;
import com.patika.weatherapi.dto.LocationResult;
import com.patika.weatherapi.service.GeocodingService;
import com.patika.weatherapi.service.WeatherService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/weather")
@RequiredArgsConstructor
public class WeatherController {
    private final GeocodingService geocodingService;
    private final WeatherService weatherService;

    @GetMapping("/daily")
    public ResponseEntity<?> getDailyWeather(@RequestParam String country, @RequestParam String city) {
        if (country == null || city == null) {
            return ResponseEntity.badRequest().body("Country and city parameters are required");
        }
        Optional<LocationResult> result = geocodingService.getCoordinates(country + ", " + city);
        if (result.isEmpty()) {
            return ResponseEntity.badRequest().body("Location not found");
        }
        ForecastResponse response = weatherService.getDailyForecast(result.get().latitude, result.get().longitude);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/weekly")
    public ResponseEntity<?> getWeeklyWeather(@RequestParam String country, @RequestParam String city) {
        if (country == null || city == null) {
            return ResponseEntity.badRequest().body("Country and city parameters are required");
        }
        Optional<LocationResult> result = geocodingService.getCoordinates(country + ", " + city);
        if (result.isEmpty()) {
            return ResponseEntity.badRequest().body("Location not found");
        }
        ForecastResponse response = weatherService.getWeeklyForecast(result.get().latitude, result.get().longitude);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/monthly")
    public ResponseEntity<?> getMonthlyWeather(@RequestParam String country, @RequestParam String city) {
        if (country == null || city == null) {
            return ResponseEntity.badRequest().body("Country and city parameters are required");
        }
        Optional<LocationResult> result = geocodingService.getCoordinates(country + ", " + city);
        if (result.isEmpty()) {
            return ResponseEntity.badRequest().body("Location not found");
        }
        ForecastResponse response = weatherService.getMonthlyForecast(result.get().latitude, result.get().longitude);
        return ResponseEntity.ok(response);
    }
}
