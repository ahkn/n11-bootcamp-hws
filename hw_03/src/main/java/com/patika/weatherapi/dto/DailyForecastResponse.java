package com.patika.weatherapi.dto;

import java.util.List;

public record DailyForecastResponse(
        List<String> time,
        List<Integer> weather_code,
        List<Double> temperature_2m_max,
        List<Double> temperature_2m_min
) {
}
