package com.patika.weatherapi.dto;

import java.util.List;

public record ForecastResponse(
        double latitude,
        double longitude,
        String timezone,
        DailyForecastResponse daily
) {}

