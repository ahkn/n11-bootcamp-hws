package com.patika.weatherapi.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class GeocodingResponse {
    @JsonProperty("results")
    private List<LocationResult> results;

    private String generationtime_ms;
}

