package com.patika.weatherapi.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LocationResult {
    @JsonProperty("latitude")
    public double latitude;

    @JsonProperty("longitude")
    public double longitude;
}
