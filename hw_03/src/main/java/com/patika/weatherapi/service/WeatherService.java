package com.patika.weatherapi.service;

import com.patika.weatherapi.dto.ForecastResponse;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class WeatherService {
    private final RestTemplate restTemplate;

    public WeatherService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public ForecastResponse getDailyForecast(double latitude, double longitude) {
        String url = "https://api.open-meteo.com/v1/forecast?latitude=" + latitude +
                "&longitude=" + longitude +
                "&daily=weather_code,temperature_2m_max,temperature_2m_min&forecast_days=1";
        return restTemplate.getForObject(url, ForecastResponse.class);
    }

    public ForecastResponse getWeeklyForecast(double latitude, double longitude) {
        String url = "https://api.open-meteo.com/v1/forecast?latitude=" + latitude +
                "&longitude=" + longitude +
                "&daily=weather_code,temperature_2m_max,temperature_2m_min&forecast_days=7";
        return restTemplate.getForObject(url, ForecastResponse.class);
    }

    public ForecastResponse getMonthlyForecast(double latitude, double longitude) {
        String url = "https://api.open-meteo.com/v1/forecast?latitude=" + latitude +
                "&longitude=" + longitude +
                "&daily=weather_code,temperature_2m_max,temperature_2m_min&forecast_days=30";
        return restTemplate.getForObject(url, ForecastResponse.class);
    }
}
