package com.patika.weatherapi.service;

import com.patika.weatherapi.dto.GeocodingResponse;
import com.patika.weatherapi.dto.LocationResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@Service
public class GeocodingService {

    private final RestTemplate restTemplate;

    @Autowired
    public GeocodingService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public Optional<LocationResult> getCoordinates(String locationName) {
        String url = String.format("https://geocoding-api.open-meteo.com/v1/search?name=%s&count=1&language=en&format=json", locationName);
        GeocodingResponse response = restTemplate.getForObject(url, GeocodingResponse.class);
        if (response != null && !response.getResults().isEmpty()) {
            return Optional.of(response.getResults().get(0));
        }
        return Optional.empty();
    }
}
