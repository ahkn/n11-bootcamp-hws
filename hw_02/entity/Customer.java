package entity;

import java.time.LocalDateTime;
import java.util.HashSet;

public class Customer {
    private long id;
    private String name;
    private String email;
    private String phone;

    private HashSet<Order> orders;

    private LocalDateTime createdAt;

    public Customer(String name, String email, String phone) {
        this.id = String.format("%s%s", name, email).hashCode();
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.orders = new HashSet<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void addOrder(Order order) {
        orders.add(order);
    }

    public HashSet<Order> getOrders() {
        return orders;
    }

    public void setOrders(HashSet<Order> orders) {
        this.orders = orders;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
}
