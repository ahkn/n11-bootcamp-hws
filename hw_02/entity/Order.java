package entity;

import java.util.ArrayList;
import java.util.List;

public class Order {
    private final List<Item> items;
    private long id;
    private Customer customer;
    private Invoice invoice;

    public Order(Customer customer) {
        this.id = System.currentTimeMillis() / 1000;
        this.customer = customer;
        this.items = new ArrayList<>();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public List<Item> getItems() {
        return items;
    }

    public void addItem(Item item) {
        this.items.add(item);
    }
}
