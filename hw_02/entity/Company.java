package entity;

import java.util.HashSet;

public class Company {
    private final String name;
    private final String sector;

    private final HashSet<Order> orders;

    public Company(String name, String sector) {
        this.name = name;
        this.sector = sector;
        this.orders = new HashSet<>();
    }

    public String getName() {
        return name;
    }

    public String getSector() {
        return sector;
    }

    public void addOrder(Order order) {
        orders.add(order);
    }

    public HashSet<Order> getOrders() {
        return orders;
    }
}
