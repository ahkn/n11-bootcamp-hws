package entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;

public class Invoice {
    private long id;

    private Company issuer;
    private String description;
    private HashMap<Item, Integer> items;
    private LocalDateTime createdAt;

    private BigDecimal amount;

    public Invoice(String description, Order order) {
        this.id = System.currentTimeMillis() / 1000;
        this.description = description;
        this.items = new HashMap<>();
        for (Item item : order.getItems()) {
            this.items.put(item, this.items.getOrDefault(item, 0) + 1);
        }
        this.createdAt = LocalDateTime.now();
        this.amount = BigDecimal.ZERO;
        this.calculateAmount();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public HashMap<Item, Integer> getItems() {
        return items;
    }

    public void setItems(HashMap<Item, Integer> items) {
        this.items = items;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    private void calculateAmount() {
        this.amount = BigDecimal.ZERO;
        for (Item item : this.items.keySet()) {
            this.amount = this.amount.add(item.getPrice().multiply(BigDecimal.valueOf(this.items.get(item))));
        }
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Company getIssuer() {
        return issuer;
    }

    public void setIssuer(Company issuer) {
        this.issuer = issuer;
    }
}
