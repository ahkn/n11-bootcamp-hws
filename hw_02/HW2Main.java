import entity.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class HW2Main {
    public static void main(String[] args) {
        List<Company> companyList = new ArrayList<>();
        List<Item> itemList = new ArrayList<>();
        List<Customer> customerList = new ArrayList<>();

        for (int i = 0; i < 10; ++i) {
            Company company = new Company("Company" + i, "sector" + i);
            companyList.add(company);
        }

        for (int i = 0; i < 10; ++i) {
            Item item = new Item("Item" + i, BigDecimal.valueOf((int) ((i + 1) * Math.random() * 2000)));
            itemList.add(item);
        }

        for (int i = 0; i < 10; ++i) {
            Customer c = new Customer("Customer" + i, "email" + i, "phone" + i);
            LocalDateTime createdAt = LocalDateTime.of(2024, (int) (1 + Math.random() * 10), 1, 0, 0);
            c.setCreatedAt(createdAt);
            customerList.add(c);

            Order o = new Order(c);
            for (int j = 0; j < 5; ++j) {
                o.addItem(itemList.get((int) (Math.random() * 10)));
            }

            c.addOrder(o);
            Company company = companyList.get((int) (Math.random() * 10));
            Invoice invoice = new Invoice("Invoice" + i, o);
            invoice.setIssuer(company);
            o.setInvoice(invoice);
            company.addOrder(o);
        }

        // List customers with name including "C"
        customerList.stream().filter(c -> c.getName().contains("C")).forEach(c -> System.out.println(c.getName()));

        // List invoices with amount greater than 1500
        customerList.stream().map(Customer::getOrders).flatMap(orders -> orders.stream()).map(Order::getInvoice).filter(invoice -> invoice.getAmount().compareTo(BigDecimal.valueOf(1500)) > 0).forEach(invoice -> System.out.println(invoice.getAmount()));

        // List customers with invoices with amount less than 500
        customerList.stream().filter(c -> c.getOrders().stream().map(Order::getInvoice).anyMatch(invoice -> invoice.getAmount().compareTo(BigDecimal.valueOf(500)) < 0)).forEach(c -> System.out.println(c.getName()));

        // List total amount of invoices for each customer registered in June
        customerList.stream().filter(c -> c.getCreatedAt().getMonthValue() == 6).map(Customer::getOrders).flatMap(orders -> orders.stream()).map(Order::getInvoice).map(Invoice::getAmount).reduce(BigDecimal.ZERO, BigDecimal::add);

        // List sectors of companies with average invoice amount less than 750 in June
        companyList.stream()
                .filter(company -> {
                    if (company.getOrders().isEmpty()) {
                        return false; // Skip companies with no orders
                    }
                    BigDecimal totalAmount = company.getOrders().stream()
                            .filter(order -> order.getInvoice().getCreatedAt().getMonthValue() == 6)
                            .map(Order::getInvoice)
                            .map(Invoice::getAmount)
                            .reduce(BigDecimal.ZERO, BigDecimal::add);
                    BigDecimal average = totalAmount.divide(BigDecimal.valueOf(company.getOrders().size()), BigDecimal.ROUND_HALF_UP);
                    return average.compareTo(BigDecimal.valueOf(750)) < 0;
                })
                .forEach(company -> System.out.println(company.getSector()));

        // Average of invoices over 1500
        customerList.stream().map(Customer::getOrders).flatMap(orders -> orders.stream()).map(Order::getInvoice).map(Invoice::getAmount).filter(amount -> amount.compareTo(BigDecimal.valueOf(1500)) > 0).mapToDouble(BigDecimal::doubleValue).average().getAsDouble();
    }
}
